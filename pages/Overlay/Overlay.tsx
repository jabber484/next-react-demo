import { NextPage } from 'next';
import { useContext, useEffect, useState } from 'react';
import { Unity } from 'react-unity-webgl';
import { IUnityContextHook } from 'react-unity-webgl/distribution/interfaces/unity-context-hook';
import { unityContextData } from '../../context/UnityContextProvider';
import styles from "./Overlay.module.css"


const Overlay : NextPage = () => {
  const unityContext : IUnityContextHook = useContext(unityContextData)
  const [dataState, setDataState] = useState("Press the button on left to get a random number");
  const [localState, setLocalState] = useState("Randomise");

  useEffect(() => {
    unityContext.addEventListener("OnUnityToReact", setDataState);
    return () => {
      unityContext.removeEventListener("OnUnityToReact", setDataState);
    };
  }, [unityContext.addEventListener, unityContext.removeEventListener, setDataState]);

  const onButtonClick = () => {
    const rng = Math.floor(Math.random() * 100);
    unityContext.sendMessage("DemoGameObject", "UpdateState", rng)
    setLocalState(rng.toString())
  }

  return (
    <div className={styles.container}>
      <div className={styles.content}>
        <h2>This is a panel in React</h2>
      </div>

      <div className={styles.content}>
        <h4>From [Unity] to [React]</h4>
        <div>{dataState}</div>
      </div>

      <div className={styles.content}>
        <h4>From [React] to [Unity]</h4>
        <button className={styles.button} onClick={onButtonClick}>
          {localState}
        </button>
      </div>

    </div>
  )
}

export default Overlay;
