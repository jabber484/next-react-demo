import { NextPage } from 'next';
import { useContext } from 'react';
import { Unity } from 'react-unity-webgl';
import { IUnityContextHook } from 'react-unity-webgl/distribution/interfaces/unity-context-hook';
import { unityContextData } from '../../context/UnityContextProvider';
import styles from "./UnityCanvas.module.css"

const UnityCanvas : NextPage = () => {
  const unityContext : IUnityContextHook = useContext(unityContextData)

  return (
    <Unity className={styles.container} unityProvider={unityContext.unityProvider} />
  )
}

export default UnityCanvas;
