import type { NextPage } from 'next'
import { UnityContextProvider } from '../context/UnityContextProvider'
import Overlay from './Overlay/Overlay'
import UnityCanvas from './UnityCanvas/UnityCanvas'

const Home: NextPage = () => {
  return <>
    <UnityContextProvider>
      <UnityCanvas />
      <Overlay />
    </UnityContextProvider>
  </>
}

export default Home
