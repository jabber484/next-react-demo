This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

The purpose of this project is to demostrate how to communicate with a WebGL build of Unity using NextJS as a container.

## Getting Started

First, run the development server:

```bash
npm run dev
# or
yarn dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

## Bridge to Unity

This project make use the following library to insert the WebGl build

https://www.npmjs.com/package/react-unity-webgl

Using its API, data can be transfer from and to each other. Although only a limit raw type is supported, it should be suffient for our scope.


## Unity Context

Most component will access the UnityContext object as a React Context, such that components don't have to pass down the object.

See `UnityContextProvider.tsx` for the Context provider

Any component that use its context can access the running Unity instance. For usage, visit `Overlay.tsx`.
