import { createContext, ReactNode, FC, useEffect } from 'react';
import { useUnityContext } from 'react-unity-webgl';
import { IUnityContextHook } from 'react-unity-webgl/distribution/interfaces/unity-context-hook';

export const unityContextData = createContext<any>(null);
export const UnityContextProvider: FC<{children : ReactNode}> = ({ children }) => {
  const context = useUnityContext({
    loaderUrl: process.env.NEXT_PUBLIC_UNITY_loaderUrl || "null",
    dataUrl: process.env.NEXT_PUBLIC_UNITY_dataUrl || "null",
    frameworkUrl: process.env.NEXT_PUBLIC_UNITY_frameworkUrl || "null",
    codeUrl: process.env.NEXT_PUBLIC_UNITY_codeUrl || "null",
  })

  return (
    <unityContextData.Provider value={context}>
      {children}
    </unityContextData.Provider>
  );
}